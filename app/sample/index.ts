import functions from "@google-cloud/functions-framework";
import { Sample } from "./sample";

export const handler: functions.HttpFunction = (_req, res) => {
  const sample = new Sample();
  return res.status(200).send(sample.add(1, 2));
};
