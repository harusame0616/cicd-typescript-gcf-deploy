import { Sample } from "../sample";

Sample
describe('Sample', () => {
    it('right case', () => {
        const sample = new Sample();
        expect(sample.add(1, 2)).toBe(3);
    });
})
