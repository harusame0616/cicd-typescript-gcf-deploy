# cicd-typescript-gcf-deploy

## About this

GitLab CI/CDを使って TypeScript プログラムを Google Cloud Functionへ
デプロイするサンプルです。

Push & マージされたタイミングで TypeScript のビルド & テストの実行 & Google Cloud Functions へデプロイします。
main へのマージの場合のみ、プロダクション環境へデプロイし、
それ以外はステージング環境へデプロイします。

GCFへデプロイするためのキーファイルは事前GitLabのCI/CD変数に登録してあります
> 設定 > CI/CD > 変数
