import { Configuration } from "webpack";
import { resolve } from "path";
import nodeExternals from "webpack-node-externals";
import CopyPlugin from "copy-webpack-plugin";

const config: Configuration = {
  target: "node",
  entry: {
    main: resolve(__dirname, "app", "sample", "index.ts"),
  },
  output: {
    path: resolve(__dirname, "artifacts"),
    filename: "[name]/index.js",
    library: {
      type: "commonjs2",
    },
  },
  externals: nodeExternals(),
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: "/node_modules",
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: "./package*.json", to: "./main/" }],
    }),
  ],
};

const buildModes = ["development", "production", "none"] as const;
type BuildMode = typeof buildModes[number];
const isBuildMode = (mode: any): mode is BuildMode => {
  return buildModes.includes(mode) || mode == undefined;
};

if (isBuildMode(process.env.NODE_ENV) || process.env.NODE_ENV == undefined) {
  config.mode = process.env.NODE_ENV || buildModes[0];
} else {
  throw new Error("ビルドモード指定エラー");
}

export default config;
